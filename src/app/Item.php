<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Cannelle;

/**
 * Description of Item
 *
 * @author alourenco
 */
class Item extends \stdClass
{

    public $id;

    public $quantity;

    public $price;

    public $label;


    function __construct($id, $quantity, $price, $label)
    {
        $this->id       = $id;
        $this->quantity = $quantity;
        $this->price    = $price;
        $this->label    = $label;

    }//end __construct()


    function getId()
    {
        return $this->id;

    }//end getId()


    function getQuantity()
    {
        return $this->quantity;

    }//end getQuantity()


    function getPrice()
    {
        return $this->price;

    }//end getPrice()


    function getLabel()
    {
        return $this->label;

    }//end getLabel()


    function setId($id)
    {
        $this->id = $id;

    }//end setId()


    function setQuantity($quantity)
    {
        $this->quantity = $quantity;

    }//end setQuantity()


    function setPrice($price)
    {
        $this->price = $price;

    }//end setPrice()


    function setLabel($label)
    {
        $this->label = $label;

    }//end setLabel()


}//end class
