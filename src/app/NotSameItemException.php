<?php

namespace Cannelle;

class NotSameItemException extends \Exception
{


    function __construct()
    {
        parent::__construct('Item with different price was added', 1);

    }//end __construct()


}//end class
