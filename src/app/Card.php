<?php

namespace Cannelle;

class Card
{

    //item  - id
    //      - quantité
    //      - nom
    //      - prix
    private $items = [];
    private $id= "MonPanier";
    private $backend;


    function __construct( \SessionHandlerInterface $backend)
    {

        // Session ->mysql/mongodb
        // Session Memcached
        // Session Redis    
        // Session PHP
        $this->backend = $backend;
        $this->backend->open("fake", "fakesession");
        $data = $backend->read($this->id);
        

    }

    function __destruct()
    {
        $this->backend->close();
    }

    /**
     * Return the number of items in the card
     * 
     * @return int Le nombre d'item dans le panier
     */
    public function getItemsCount(): int
    {
        return count($this->items);
    }

    /**
     * Return number of products in card
     *
     * @return int
     */
    public function getCount(): int
    {
        $nb = 0;
        /* @var $item Item */
        foreach ($this->items as $key => $item) {
            $nb += $item->getQuantity();
        }
        return $nb;
    }
/**
 * 
 */
    public function addItem(Item $item)
    {
        foreach ($this->items as $key => $it) {
            if ($item->getId() == $it->getId()) {
                if ($item->getPrice() != $it->getPrice()) {
                    throw new NotSameItemException();
                }
                $item->setQuantity($item->getQuantity() + $it->getQuantity());
                return;
            }
        }
        $this->items[] = $item;

    }

    public function getItem(int $id): Item
    {
    }

    public function getArrayItem(int $id): array
    {
    }

    public function removeItem(Item $item)
    {
        //...implementation...
        $this->save();
    }

    public function deleteItem(int $id)
    {
        //... implementation ...
        // save abs !!
    }

    public function clear()
    {
        $this->items = [];
        $this->save();
    }

    public function save()
    {
        $this->backend->write($this->id ,$this->items);
    }
}
