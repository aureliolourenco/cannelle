<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Cannelle\Backend;

/**
 * Description of MySQLBackEnd
 *
 * @author alourenco
 */
class MySQLBackEnd implements \SessionHandlerInterface
{
    //put your code here
    public function close()
    {
    }

    public function destroy($session_id)
    {
    }

    public function gc($maxlifetime)
    {
    }

    public function open($save_path,  $session_name)
    {
    }
    /**
     * read a string (serialized object)
     * @param $id Session id
     * @return string
     */
    public function read($session_id)
    {
    }

    /**
     * Write string (serialized data/object) to backend
     * @param $session_id  session id
     * @param $session_data data to be saved
     * 
     * @return bool
     */
    public function write($session_id,  $session_data)
    {
    }
}
