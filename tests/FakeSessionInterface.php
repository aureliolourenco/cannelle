<?php

namespace Cannelle ;

class FakeSessionInterface implements \SessionHandlerInterface {
    public function close(): bool {
        
    }

    public function destroy(string $session_id): bool {
        
    }

    public function gc(int $maxlifetime): bool {
        
    }

    public function open(string $save_path, string $session_name) {
        
    }

    public function read(string $session_id): string {
        //une implementation
        return "string correspondant au besoin" ; 
    }

    public function write(string $session_id, string $session_data): bool {
        
    }
}