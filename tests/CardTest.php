<?php

namespace Test;

use \PHPUnit\Framework\TestCase;
use \Cannelle\Card;
use Cannelle\DummySessionInterface;
use Cannelle\FakeSessionInterface;
use \Cannelle\Item;
use \Cannelle\NotSameItemException;

class CardTest extends TestCase
{

  /* @var  $card Card*/
  protected $card;
  protected $it1;
  protected $it2;
  protected $it3;
  protected $it4;
  protected $it5;

  public static function setUpBeforeClass()
  {
    fwrite(STDOUT, __METHOD__ . "\n");
  }


  protected function setUp()
  {

    $stub = $this->createMock("\SessionHandlerInterface");
    //$stub->expects($this->any())->method('doSomething')->willReturn('foo'
    $stub->expects($this->once())->method("open")->willReturn("");

    $this->card = new Card($stub);

    $this->it1 = new Item(1, 4, 10, "pommes");
    $this->it2 = new Item(1, 2, 10, "pommes");
    $this->it3 = new Item(2, 4, 5, "poires");
    $this->it4 = new Item(2, 8, 5, "poires");
    $this->it5 = new Item(1, 3, 5, "pommes");
  }

  /**
   * @test 
   */
  public function check_new_card_is_empty()
  {
    $this->assertEquals(0, $this->card->getItemsCount());
  }

  /**
   * @test
   */
  public function chech_card_contains_one_item()
  {
    $this->card->addItem($this->it1);
    $this->assertEquals(1, $this->card->getItemsCount());
  }

  /**
   * @test
   */
  public function chech_card_contains_two_items()
  {
    // $this->markTestIncomplete(
    //    'This test has not been implemented yet.'
    //  );

    $this->card->addItem($this->it1); // 4 pommes
    $this->card->addItem($this->it3); // 4 poires

    $this->assertEquals(2, $this->card->getItemsCount()); // 2 'articles'
    $this->assertEquals(8, $this->card->getCount());  // 8 fruits

  }

  /**
   * Adding 2 times apples results in having 1 'pack' of apples
   * @test
   */
  public function chech_card_contains_one_items_who_is_8_apples()
  {
    $this->card->addItem($this->it1); // 4 pommes
    $this->card->addItem($this->it1); //4 pommes a nouveau

    $this->assertEquals(1, $this->card->getItemsCount()); //un pack de pommes
    $this->assertEquals(8, $this->card->getCount()); //8 pommes en tout

  }

  /**
   * @test
   */
  public function check_card_is_cleared()
  {
    // Stop here and mark this test as incomplete.
    //$this->markTestIncomplete(
    //  'This test has not been implemented yet.'
    //);
    $this->card->addItem($this->it1);
    $this->assertEquals(1, $this->card->getItemsCount());
    $this->card->clear();
    $this->assertEquals(0, $this->card->getItemsCount(), "Le panier devrait etre vide !!");
  }
  /**
   * @test
   * @expectedException \Cannelle\NotSameItemException
   * @expectedExceptionCode 1
   */
  public function should_raise_NotSameItemException()
  {
    //$this->expectException(NotSameItemException::class) ;

    $this->card->addItem($this->it1);
    $this->card->addItem($this->it5);
  }


  /**
   * @test
   */
  public function check_item_is_removed()
  {

    $stub = $this->createMock("\SessionHandlerInterface");
    $stub->expects($this->once())->method("open")->willReturn("");
    $stub->expects($this->once())->method("write")->willReturn(true);

    $this->card = new Card($stub);
    
    $this->card->addItem($this->it1);
    $this->card->removeItem($this->it1);
    
  }
}
