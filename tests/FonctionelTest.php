<?php

namespace Test;

use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\WebDriverBy;

use \PHPUnit\Framework\TestCase;

class FonctionnelTest extends TestCase
{

    private $driver;

    public function setup()
    {

        $host = 'http://localhost:4444/wd/hub';


        $options       = new ChromeOptions();
        $options->addArguments(["window-size=400,400"]); //FIXME: ne fonctionne plus


        $desiredCapabilities = DesiredCapabilities::chrome();
        $desiredCapabilities->setCapability(ChromeOptions::CAPABILITY, $options);

        $this->driver = RemoteWebDriver::create($host, DesiredCapabilities::chrome());
    }
    /**
     * @test
     */
    public function check_page_title()
    {
        $this->driver->get("http://127.0.0.1:8888");
        $this->assertContains("Demo", $this->driver->getTitle());
    }

    /**
     * @test
     */
    public function check_greeting()
    {
        $this->driver->get("http://127.0.0.1:8888");
        $element = $this->driver->findElement(WebDriverBy::id('msg'))->getText();
        $this->assertContains("Hello", $element);
    }

    /**
     * @test
     */
    public function check_google_link()
    {
        $this->driver->get("http://127.0.0.1:8888");
        $this->driver->findElement(WebDriverBy::partialLinkText("google"))->click();
        $this->driver->wait(30);
        $this->assertContains("Google", $this->driver->getTitle());

    }

    public function tearDown()
    {
        $this->driver->close();
    }
}
