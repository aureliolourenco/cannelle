<?php

namespace Cannelle ;

class DummySessionInterface implements \SessionHandlerInterface {
    public function close()  {
        
    }

    public function destroy( $session_id) {
        
    }

    public function gc( $maxlifetime) {
        
    }

    public function open(string $save_path, string $session_name) {
        
    }

    public function read(string $session_id): string {
        
    }

    public function write(string $session_id, string $session_data): bool {
        
    }
}